FROM debian:8.4

MAINTAINER steffen lorenz <slorenz@noreplay.com>

RUN apt-get update -y && apt-get upgrade -y
RUN apt-get install -y git openjdk-7-jre-headless wget
RUN useradd -ms /bin/bash gerrit2

ENV GERRIT_WAR gerrit-2.2.2.war
ENV NO_START 1

USER gerrit2
WORKDIR /home/gerrit2

RUN mkdir .ssh && /bin/chmod 0700 .ssh
RUN ssh-keygen -q -t rsa -N '' -f .ssh/id_rsa
RUN /usr/bin/wget https://www.gerritcodereview.com/download/$GERRIT_WAR
RUN /bin/chmod +x $GERRIT_WAR
RUN mv $GERRIT_WAR gerrit.war
RUN ["/usr/bin/java", "-jar", "gerrit.war", "init", "--batch", "-d", "/home/gerrit2/gerrit_project"]

USER root
COPY gerrit.config gerrit_project/etc/gerrit.config
RUN /bin/chown gerrit2:gerrit2 gerrit_project/etc/gerrit.config

EXPOSE 8080

CMD ["/usr/bin/java", "-jar", "gerrit.war", "daemon", "-d", "/home/gerrit2/gerrit_project"]
