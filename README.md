Usage Info
==========

This gerrit image includes basic setup as described on: [Gerrit Documentation](https://www.gerritcodereview.com).
You can use it for testing purposes. See Dockerfile for current steps!

Example
-------

$ docker pull slorenz/gerrit

$ docker run -d -p 8080:8080 --name gerrit_container slorenz/gerrit

$ docker stop gerrit_container

$ docker start gerrit_container

Contribution
------------

If you like you can contribute on [Bitbucket](https://bitbucket.org/Lupin3000/gerrit).

$ mkdir gerrit && cd gerrit

$ git remote add origin https://Lupin3000@bitbucket.org/Lupin3000/gerrit.git

$ git add Dockerfile

$ git commit -m '<description>'

$ git push -u origin master
